#!/bin/bash
trap "exit" 2 3
declare -A pid
declare -a hosts_name

hosts_file='/etc/hosts'
adminer_port=9042

host_id=0
port_selected=0
host_selected=""
path_selected=""

err() {
    echo "FATAL ERROR: ${1}"
    exit 1
}

get_hosts() {
    local _i=0

    hosts_name[0]="0.0.0.0"
    echo "(${_i}) [all] ${hosts_name[${_i}]}"
    ((++_i))

    while read -r _line; do
        if [[ $_line == '#'* ]]; then
            continue
        fi

        hosts_name[${_i}]="${_line##* }"
        echo "(${_i}) ${hosts_name[${_i}]}"

        ((++_i))
    done < "${hosts_file}"
}

check_host() {
    if [[ ${hosts_name[${host_id}]} == "" ]]; then
        host_selected="0.0.0.0"
        return
    fi

    host_selected="${hosts_name[${host_id}]}"
}

check_port() {
    if [[ ${port_selected} == "" ]]; then
        port_selected="8080"
    fi
}

check_path() {
    if [[ ${path_selected} == "" ]]; then
        path_selected="$(pwd)"
        return
    fi

    if [[ ! -e ${path_selected} ]]; then
        echo "Impossible d'utiliser \`${path_selected}\`, utilisation du path par default"
        path_selected="$(pwd)"
        return
    fi
}

run_adminer() {
    echo "Vous pouvez gerer MYSQL depuis \`http://127.0.0.1:${adminer_port}\`"
    php -S "0.0.0.0:${adminer_port}" -t "/etc/adminer/" > /dev/null 2>&1 &
    pid['adminer']="$!"
    
    kill -0 "${pid['adminer']}" || echo 'PHP Mysql a rencontre une erreur, une autre instance est peut etre en route'
}

run_instance() {
    echo -e "Votre instance tourne sur \`http://${host_selected}:${port_selected}\`\n"
    php -S "${host_selected}:${port_selected}" -t "${path_selected}"
    pid['instance']="$!"
}

exit() {
    echo -e '\nArret des services en cours'
    kill -9 "${pid['instance']}" > /dev/null 2>&1
    kill -9 "${pid['adminer']}" > /dev/null 2>&1
}

echo -e "List des Hosts disponible:\n(Pour en ajouter un \`sudo echo \"127.0.1.1 [NAME].localdomain [NAME]\" >> /etc/hosts\`)"
get_hosts
echo -ne "\nVous utiliserez (0): "
read -r host_id
check_host

echo -ne "\nSur quel port (8080): "
read -r port_selected
check_port

echo -ne "\nPhp va cibler \`$(pwd)\`\nEntrer un autre path si besoin: "
read -r path_selected
check_path

echo -e "\nLencement de mariadb"
sudo systemctl restart mariadb
run_adminer

echo -e "\nDebut de l'instance PHP"
run_instance