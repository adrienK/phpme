# phpme
Verry simple and easy to use
- Non production usage
- Easy php and db usage
- No config required

If you are lazy (you need curl) ... (like me), run this install script for make automaticaly there step (Prepare DEBIAN, Installing it):     
`curl https://git.iglou.eu/adrien/phpme/raw/branch/master/lazy_install.sh | bash -s --`

## Prepare DEBIAN
**Install PHP anf MariaDB**
```
sudo apt update
sudo apt install php php-mysql mariadb-server
```

**Install classic php mod**
```
sudo apt install php-curl php-gd php-intl php-json php-mbstring php-xml php-zip
```

**Config MariaDB** *(user: admin, password: admin)*
```
sudo systemctl start mysql
sudo mysql -u root -p -e 'CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin'; GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'; FLUSH PRIVILEGES;'
```
## Installing it
```
sudo mkdir -p /etc/adminer
sudo wget 'https://github.com/vrana/adminer/releases/download/v4.7.7/adminer-4.7.7.php' -O '/etc/adminer/index.php'
sudo wget 'https://raw.githubusercontent.com/Niyko/Hydra-Dark-Theme-for-Adminer/master/adminer.css' -O '/etc/adminer/adminer.css'

sudo wget 'https://git.iglou.eu/adrien/phpme/raw/branch/master/phpme.sh' -O '/usr/local/bin/phpme'
sudo chmod +x /usr/local/bin/phpme
sudo chown root:root /usr/local/bin/phpme
```

## Using it
- Go to your php repository
- Start phpme `$ phpme`
- Go to work ...